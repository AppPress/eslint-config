"use strict";

module.exports = {
	extends: ["./base", "./rules/flow", "./rules/react", "./rules/prettier"].map(
		require.resolve
	),
	env: {
		browser: true,
	},
	globals: {
		__DEV__: true,
		module: false,
	},
	parserOptions: {
		sourceType: "module",
		ecmaVersion: 2017,
		ecmaFeatures: {
			experimentalObjectRestSpread: true,
		},
	},
};

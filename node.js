"use strict";

module.exports = {
	extends: ["./base", "./rules/node", "./rules/prettier"].map(require.resolve),
	rules: {},
};

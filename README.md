# eslint-config

This package provides App Press' .eslintrc.json as an extensible shared config.

## Usage
A few ESLint configurations are exported for convenience; all exports allow for standard testing frameworks (Mocha, Jasmine, and Jest):

### eslint-config
The default export contains all ESLint rules, including ES6 and React, and tries to find a balance between traditional server-side and client-side code styles. For this reason, it does not use modules for client-side code by default; thus, the following code will not pass linting:

```js
import React from "react";
```

But this will:

```js
const React = require("react");
```

1. `npm install --save-dev @app-press/eslint-config eslint eslint-plugin-react`
1. Add `"extends": "@app-press/eslint-config` to your .eslintrc file

### eslint-config/node
This export is for use on server-side-only code and will not lint React (though it will lint ES6).

1. `npm install --save-dev @app-press/eslint-config eslint`
1. Add `"extends": "@app-press/eslint-config/node` to your .eslintrc.json file

### eslint-config/client
This export is for use on client-side-only code and will lint React (including JSX) and ES6 code.

1. `npm install --save-dev @app-press/eslint-config eslint`
1. Add `"extends": "@app-press/eslint-config/client` to your .eslintrc file

### eslint-config/client-modules
This export is for use on client-side-only code and will lint React (including JSX) and ES6 code. It will also expect that all client-side code is ES6 module based (and thus will assume strict by default).

1. `npm install --save-dev @app-press/eslint-config eslint`
1. Add `"extends": "@app-press/eslint-config/client-modules` to your .eslintrc file

**To override specific rules**, simply add a `rules` section to your .elintrc.json and add your desired rules — they will take precedence.

You may also modify the parser, parserOptions, etc. Just include those sections as well.

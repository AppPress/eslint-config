<a name="2.0.0"></a>
# [2.0.0](https://bitbucket.org/AppPress/eslint-config/compare/1.5.5...v2.0.0) (2017-07-10)


### Features

* update and reconfigure rules/exports ([b8a8d3a](https://bitbucket.org/AppPress/eslint-config/commits/b8a8d3a))


### BREAKING CHANGES

* The default export is now node 6+ rather than node 4. There are also new
rules that will cause errors in some projects. Modules previously listed
as dependencies are now listed as peer dependencies (as per the
community standard).




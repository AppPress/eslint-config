"use strict";

module.exports = {
	rules: {
		// enforce spacing inside array brackets
		"array-bracket-spacing": 2,

		// enforce one true brace style
		"brace-style": [2, "1tbs"],

		// enforce spacing after comma
		"comma-spacing": [2, { before: false, after: true }],

		// enforce one true comma style
		"comma-style": [2, "last"],

		// disallow spacing inside of computed properties
		"computed-property-spacing": [2, "never"],

		// enforce consistent naming when capturing the execution context
		"consistent-this": [2, "self"],

		// enforce one new line at the end of a file
		"eol-last": 2,

		// recommend function expressions to have names
		"func-names": 1,

		// enforce tabs and widths
		indent: [2, "tab", { SwitchCase: 1 }],

		// prefer double quotes in jsx
		"jsx-quotes": 2,

		// enforces spacing between keys and values in object literal properties
		"key-spacing": [2, { beforeColon: false, afterColon: true }],

		// require spaces after keywords
		"keyword-spacing": [2, { after: true }],

		// enforce linebreak style
		"linebreak-style": 2,

		// require a capital letter for constructors
		"new-cap": 2,

		// disallow use of the Array constructor
		"no-array-constructor": 2,

		// disallow use of bitwise operators
		"no-bitwise": 2,

		// disallow empty blocks
		"no-empty": 2,

		// disallow extra parentheses
		"no-extra-parens": 2,

		// disallow the use of irregular whitespace
		"no-irregular-whitespace": 2,

		// disallow the mixing of spaces and tabs
		"no-mixed-spaces-and-tabs": 2,

		// disallow mutliple empty lines and only one new line at the end
		"no-multiple-empty-lines": [2, { max: 2, maxEOF: 1 }],

		// disallow the use of nested ternaries

		"no-nested-ternary": 2,
		// disallow the use of the Object constructor

		"no-new-object": 2,
		// disallow the use of spaces in function calls

		"no-spaced-func": 2,

		// disallow trailing spaces
		"no-trailing-spaces": 2,

		// disallow the use of Boolean literlas in conditional expressions
		// also, prefer `a || b` over `a ? a : b`
		"no-unneeded-ternary": [2, { defaultAssignment: false }],

		// disallow whitespace before properites
		"no-whitespace-before-property": 2,

		// enforce spacing before and after curly braces
		"object-curly-spacing": [2, "always"],

		// allow just one var statement per function
		"one-var": [2, "never"],

		// require a new line around var declaration
		"one-var-declaration-per-line": [2, "always"],

		// enforce operators to be placed after linebreaks
		"operator-linebreak": [2, "after"],

		// require quotes around object literal property names
		"quote-props": [2, "as-needed", { keywords: true }],

		// enforce quote style
		quotes: [2, "double"],

		// require spacing before and after semicolons
		"semi-spacing": [2, { before: false, after: true }],

		// require the use of semicolons
		semi: [2, "always"],

		// require the use of space before blocks
		"space-before-blocks": 2,

		// require the use of space before function parentheses
		"space-before-function-paren": [2, { anonymous: "always", named: "never" }],

		// disallow spaces in parentheses
		"space-in-parens": 2,

		// requiring spacing of infix operations
		"space-infix-ops": 2,

		// require spacing after unary operations
		"space-unary-ops": 2,
	},
};

"use strict";

module.exports = {
	rules: {
		// disallow self-assignment
		"no-self-assign": 2,

		// disallow declaration of variables already declared in the outer scope
		"no-shadow": [1, { allow: ["cb", "err"] }],

		// disallow declaration of restricted keyword names
		"no-shadow-restricted-names": 2,

		// disallow use of undeclared variables unless mentioned in a global block
		"no-undef": 2,

		// disallow declaration of variables that are not used in the code
		"no-unused-vars": ["error", { argsIgnorePattern: "^_" }],

		// disallow use of variables before they are defined
		"no-use-before-define": [2, { functions: false }],
	},
};

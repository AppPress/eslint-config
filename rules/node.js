"use strict";

module.exports = {
	plugins: ["node"],
	env: {
		node: true,
	},
	rules: {
		// disallow use of new operator with the require function
		"no-new-require": 2,

		// disallow string concatenation with __dirname and __filename
		"no-path-concat": 2,

		// disallow using unsupported features based on the node version in the engines
		// field. if no engine is specified, this rule is ignored
		"node/no-unsupported-features": 2,

		// disallow use of deprecated node APIS to future-proof code
		"node/no-deprecated-api": 2,
	},
};

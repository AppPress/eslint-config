"use strict";

module.exports = {
	env: {
		es6: true,
	},
	parserOptions: {
		ecmaVersion: 6,
	},
	rules: {
		// enforces no braces where they can be omitted
		"arrow-body-style": [2, "as-needed"],

		// always require parentheses in arrow functions
		"arrow-parens": 2,

		// require spaces before and after arrow functions
		"arrow-spacing": 2,

		// verify super() calls in constructors
		"constructor-super": 2,

		// warn against modifying class variables
		"no-class-assign": 1,

		// disallow confusing arrow functions
		"no-confusing-arrow": [2, { allowParens: true }],

		// disallow modifying const variable references
		"no-const-assign": 2,

		// disallow duplicate class members
		"no-dupe-class-members": 2,

		// disallow importing from the same path more than once
		// NOTE: replaced with eslint-plugin-imports/no-duplicates
		"no-duplicate-imports": 0,

		// disallow the symbol constructor
		"no-new-symbol": 2,

		// disallow the use of `this` before calling `super()` in constructors
		"no-this-before-super": 2,

		// disallow useless computed property keys
		"no-useless-computed-key": 2,

		// disallow empty constructors
		"no-useless-constructor": 2,

		// disallow renamin gimport, export, and destructured assignments to the same name
		"no-useless-rename": 2,

		// disallow the use of var
		"no-var": 2,

		// always require object shorthand when possible
		"object-shorthand": [2, "always"],

		// require using arrow functions as callbacks
		"prefer-arrow-callback": [2, { allowNamedFunctions: true }],

		// require constant use when variable isn't reassigned
		"prefer-const": [2, { destructuring: "all" }],

		// disallow parseInt(string, radix) in favor of binary, octal, and hexadecimal literals
		"prefer-numeric-literals": 2,

		// require using rest params over arguments
		"prefer-rest-params": 2,

		// require using template strings over concatenation
		"prefer-template": 2,

		// prefer using spread over .apply()
		"prefer-spread": 1,

		// disallow generator functions that do not have yield
		"require-yield": 2,

		// disallow spaces between rest operator and expression
		"rest-spread-spacing": 2,

		// disallow spacing in curly braces of template strings
		"template-curly-spacing": 2,

		// enforce spacing around the * in yield* expressions
		"yield-star-spacing": [2, "after"],
	},
};

"use strict";

module.exports = {
	rules: {
		// enforce use of global `use strict`
		strict: [2, "global"],
	},
};

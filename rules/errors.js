"use strict";

module.exports = {
	rules: {
		// disallow the use of assignments in conditional statements
		"no-cond-assign": [2, "except-parens"],

		// disallow the use of debugger
		"no-debugger": 2,

		// disallow duplicate arguments in functions
		"no-dupe-args": 2,

		// prevent duplicate declaration of class methods
		"no-dupe-class-members": 2,

		// disallow duplicate keys when creating objects
		"no-dupe-keys": 2,

		// ensure that the results of typeof are compared against a valid string
		"valid-typeof": 2,
	},
};

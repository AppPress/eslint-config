"use strict";

module.exports = {
	parser: "babel-eslint",
	plugins: ["flowtype"],
	rules: {
		// disallow importing from the same path more than once
		// must import types separately
		"no-duplicate-imports": 0,

		// requiring spacing of infix operations
		// bugged with flow optional types
		"space-infix-ops": 0,

		// The use of bitwise operators in JavaScript is very rare and often & or | is simply a mistyped && or ||
		// flow uses bitwise operators
		"no-bitwise": 0,

		// Require Radix Parameter
		radix: 0,
	},
};

"use strict";

module.exports = {
	extends: ["prettier", "prettier/flowtype", "prettier/react"],
	plugins: ["prettier"],
	rules: {
		"prettier/prettier": [
			"error",
			{
				useTabs: true,
				printWidth: 80,
				tabWidth: 2,
				singleQuote: false,
				trailingComma: "es5",
				bracketSpacing: true,
				jsxBracketSameLine: false,
				parser: "flow",
				semi: true,
			},
		],
	},
};

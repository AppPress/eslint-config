"use strict";

module.exports = {
	env: {
		es6: true,
	},
	parserOptions: {
		ecmaVersion: 6,
	},
	plugins: ["import"],
	settings: {
		"import/resolver": {
			node: {
				extensions: [".js", ".json"],
			},
		},
		"import/extensions": [".js", ".jsx"],
		"import/ignore": [
			"node-modules",
			"\\.(coffee|scss|css|less|hbs|svg|json)$",
		],
	},
	rules: {
		// Static analysis:

		// ensure imports point to files/modules that can be resolved
		"import/no-unresolved": [2, { commonjs: true, caseSensitive: true }],

		// Helpful warnings:

		// disallow invalid exports (e.g., multiple defaults)
		"import/export": 2,

		// do not allow a default import name to match a named export
		"import/no-named-as-default": 2,

		// do not allow accessing default export property names that are also named exports
		"import/no-named-as-default-member": 2,

		// forbid mutable exports
		"import/no-mutable-exports": 2,

		// Style guide

		// disallow non-import statements appearing before import statements
		"import/first": [2, "absolute-first"],

		// disallow duplicate imports
		"import/no-duplicates": 2,

		// ensure consistent use of file extension within import path
		"import/extensions": [2, "always", { js: "never", jsx: "never" }],

		// suggest import order
		"import/order": [1, { "newlines-between": "never" }],

		// require a newline after the last import/require
		"import/newline-after-import": 2,

		// require modules with a single export to use a default export
		"import/prefer-default-export": 2,

		// forbid import of modules using absolute paths
		"import/no-absolute-path": 2,

		// forbid require() calls with expressions
		"import/no-dynamic-require": 2,

		// forbid webpack loader syntax in imports
		"import/no-webpack-loader-syntax": 2,

		// prevent importing the fault as if it were named
		"import/no-named-default": 2,
	},
};

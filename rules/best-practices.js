"use strict";

module.exports = {
	rules: {
		// treat var statements as though they were block scoped
		"block-scoped-var": 2,

		// specify curly brace conventions for all control statements
		curly: [2, "all"],

		// recommend default case in switch statements
		"default-case": 1,

		// encourages the use of dot notation where possible
		"dot-notation": [2, { allowKeywords: true }],

		// require the use of === and !==
		eqeqeq: [2, "allow-null"],

		// make sure for-in loops have an if statement
		"guard-for-in": 2,

		// disallow the use of arguments.caller or .callee
		"no-caller": 2,

		// disallow lexical declarations in case/default clauses
		"no-case-declarations": 1,

		// disallow else after a return in an if
		"no-else-return": 2,

		//  disalow empty destructuring patterns
		"no-empty-pattern": 2,

		// disallow use of eval()
		"no-eval": 2,

		// disallow unneccssary function binding
		"no-extra-bind": 2,

		// disallow fallthrough of case statements
		"no-fallthrough": 2,

		// disallow the use of leading or trailing decimal points in numeric literals
		"no-floating-decimal": 2,

		// disallow the use of eval()-like methods
		"no-implied-eval": 2,

		// disallow use of __iterator__ property
		"no-iterator": 2,

		// disallow unnecessary nested blocks
		"no-lone-blocks": 2,

		// disallow creation of functions within loops
		"no-loop-func": 2,

		// disallow use of multiple spaces
		"no-multi-spaces": 2,

		// disallow multiline strings without \n
		"no-multi-str": 2,

		// disallow reassignments of native objects
		"no-native-reassign": 2,

		// disallow the use of the new operator when not part of the assignment or comparison
		"no-new": 2,

		// disallow the use of old style octal literals
		"no-octal": 2,

		// disallow use of octal escape sequences in string literals
		"no-octal-escape": 2,

		// disallow usage of __proto__ property
		"no-proto": 2,

		// disallow declaring the same variable more than once
		"no-redeclare": 2,

		// disallow use of assignment in return statements
		"no-return-assign": 2,

		// disallow use of `javascript:` urls`
		"no-script-url": 2,

		// disallow comparisons where both sides are exactly the same
		"no-self-compare": 2,

		// disallow use of comma operator
		"no-sequences": 2,

		// disallow unnecessary string escaping
		"no-useless-escape": 2,

		// disallow use of the with statement
		"no-with": 2,

		// require the use of the second argument for parseInt()
		radix: 2,

		// require immediate function invocation to be wrapped in parentheses
		"wrap-iife": [2, "inside"],

		// disallow Yoda conditions
		yoda: 2,
	},
};

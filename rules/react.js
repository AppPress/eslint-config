"use strict";

module.exports = {
	plugins: ["react", "react-native"],
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
			experimentalObjectRestSpread: true,
		},
		settings: {
			react: {
				createClass: "createClass", // Regex for Component Factory to use, default to "createClass"
				pragma: "React", // Pragma to use, default to "React"
				version: "15.0", // React version, default to the latest React stable release
			},
		},
	},
	rules: {
		// Constructors of derived classes must call super(). Constructors of non derived classes must not call super()
		"constructor-super": 2,

		// disallow use of variables before they are defined
		"no-unused-vars": [
			2,
			{ argsIgnorePattern: "props|store|next|action|getState|state|^_" },
		],

		// disallow extra parentheses
		"no-extra-parens": 0,

		// disallow use of variables before they are defined
		"no-use-before-define": 0,

		// require modules with a single export to use a default export
		"import/prefer-default-export": 0,

		// suggest import order
		"import/order": 0,

		// Prevent missing displayName in a React component definition
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/display-name.md
		"react/display-name": [1, { ignoreTranspilerName: false }],

		// Forbid certain propTypes (any, array, object)
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/forbid-prop-types.md
		"react/forbid-prop-types": [1, { forbid: ["any"] }],

		// Enforce boolean attributes notation in JSX
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-boolean-value.md
		"react/jsx-boolean-value": [2, "always"],

		// Validate closing bracket location in JSX
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-closing-bracket-location.md
		"react/jsx-closing-bracket-location": [2, "tag-aligned"],

		// Enforce or disallow spaces inside of curly braces in JSX attributes
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-curly-spacing.md
		"react/jsx-curly-spacing": [1, "never", { allowMultiline: true }],

		// Enforce event handler naming conventions in JSX
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-handler-names.md
		"react/jsx-handler-names": [0, {}],

		// Validate props indentation in JSX
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-indent-props.md
		"react/jsx-indent-props": [2, "tab"],

		// Validate JSX has key prop when in array or iterator
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-key.md
		"react/jsx-key": 2,

		// Limit maximum of props on a single line in JSX
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-max-props-per-line.md
		"react/jsx-max-props-per-line": [1, { maximum: 3 }],

		// Prevent usage of .bind() and arrow functions in JSX props
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-bind.md
		"react/jsx-no-bind": [
			1,
			{
				allowArrowFunctions: true,
				ignoreRefs: true,
				allowBind: false,
			},
		],

		// Prevent duplicate props in JSX
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-duplicate-props.md
		"react/jsx-no-duplicate-props": [2, { ignoreCase: false }],

		// Prevent usage of unwrapped JSX strings
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-literals.md
		"react/jsx-no-literals": 0,

		// Disallow undeclared variables in JSX
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-undef.md
		"react/jsx-no-undef": 2,

		// Enforce PascalCase for user-defined JSX components
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-pascal-case.md
		"react/jsx-pascal-case": 1,

		// Enforce propTypes declarations alphabetical sorting
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-sort-prop-types.md
		"react/jsx-sort-prop-types": [
			0,
			{
				ignoreCase: false,
				callbacksLast: false,
			},
		],

		// Enforce props alphabetical sorting
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-sort-props.md
		"react/jsx-sort-props": [
			0,
			{
				ignoreCase: false,
				callbacksLast: false,
			},
		],

		// Prevent React to be incorrectly marked as unused
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-uses-react.md
		"react/jsx-uses-react": 2,

		// Prevent variables used in JSX to be incorrectly marked as unused
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-uses-vars.md
		"react/jsx-uses-vars": 2,

		// Prevent usage of dangerous JSX properties
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-danger.md
		"react/no-danger": 1,

		// Prevent usage of deprecated methods
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-deprecated.md
		"react/no-deprecated": 1,

		// Prevent usage of setState in componentDidMount
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-did-mount-set-state.md
		"react/no-did-mount-set-state": [2],

		// Prevent usage of setState in componentDidUpdate
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-did-update-set-state.md
		"react/no-did-update-set-state": [2, "disallow-in-func"],

		// Prevent direct mutation of this.state
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-direct-mutation-state.md
		"react/no-direct-mutation-state": 0,

		// Prevent usage of isMounted
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-is-mounted.md
		"react/no-is-mounted": 2,

		// Prevent multiple component definition per file
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-multi-comp.md
		"react/no-multi-comp": 0,

		// Prevent usage of setState
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-set-state.md
		"react/no-set-state": 0,

		// Prevent using string references
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-string-refs.md
		"react/no-string-refs": 1,

		// Prevent usage of unknown DOM property
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-unknown-property.md
		"react/no-unknown-property": 2,

		// Require ES6 class declarations over React.createClass
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prefer-es6-class.md
		"react/prefer-es6-class": [2, "always"],

		// Prevent missing props validation in a React component definition
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prop-types.md
		// disabled until flow types are checked
		"react/prop-types": [0, { ignore: ["style"], customValidators: [] }],

		// Prevent missing React when using JSX
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/react-in-jsx-scope.md
		"react/react-in-jsx-scope": 2,

		// Prevent extra closing tags for components without children
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/self-closing-comp.md
		"react/self-closing-comp": 2,

		// Enforce component methods order
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/sort-comp.md
		"react/sort-comp": [
			1,
			{
				order: [
					"propTypes",
					"props",
					"state",
					"lifecycle",
					"/^on.+$/",
					"/^(get|set)(?!(InitialState$|DefaultProps$|ChildContext$)).+$/",
					"everything-else",
					"/^render.+$/",
					"render",
				],
			},
		],

		// Prevent missing parentheses around multilines JSX
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-wrap-multilines.md
		"react/jsx-wrap-multilines": [
			2,
			{
				declaration: true,
				assignment: true,
				return: true,
			},
		],

		// Ensure correct position of the first property
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-first-prop-new-line.md
		"react/jsx-first-prop-new-line": [2, "multiline"],

		// Enforce stateless React Components to be written as a pure function
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prefer-stateless-function.md
		"react/prefer-stateless-function": 1,

		// Enforce ES5 or ES6 class for returning value in render function
		// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/require-render-return.md
		"react/require-render-return": 2,

		// Warn against inline styles
		"react-native/no-inline-styles": 1,

		// Disallow unused style definitions
		"react-native/no-unused-styles": 2,
	},
};

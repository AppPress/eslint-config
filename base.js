"use strict";

module.exports = {
	root: true,
	extends: [
		"./rules/best-practices",
		"./rules/errors",
		"./rules/style",
		"./rules/variables",
		"./rules/es6",
		"./rules/imports",
	].map(require.resolve),
	env: {
		es6: true,
		jasmine: true,
		jest: true,
		mocha: true,
	},
	parserOptions: {
		ecmaVersion: 6,
	},
	globals: {},
	rules: {},
};
